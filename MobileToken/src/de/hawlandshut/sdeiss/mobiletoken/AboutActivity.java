/*
 * 
 * This file is a part of Secure Mobile Token.
 * Copyright (C) 2015 by Sebastian Deiss, all rights reserved. 
 * 
 */

package de.hawlandshut.sdeiss.mobiletoken;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Display licensing information.
 * 
 * @author Sebastian Deiss
 *
 */
public class AboutActivity extends Activity
{
	private final String TAG = "AboutActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		Log.d(TAG, "Displaying licensing information");
		final TextView licenseText = (TextView)findViewById(R.id.licenseScrollView);
		// Enable scrolling on the text view
		licenseText.setMovementMethod(new ScrollingMovementMethod());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}
}
