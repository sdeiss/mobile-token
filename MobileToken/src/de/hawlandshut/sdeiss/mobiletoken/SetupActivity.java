/*
 * 
 * This file is a part of Secure Mobile Token.
 * Copyright (C) 2015 by Sebastian Deiss, all rights reserved. 
 * 
 */


package de.hawlandshut.sdeiss.mobiletoken;

import java.security.InvalidKeyException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import de.hawlandshut.sdeiss.mobiletoken.crypto.CryptoProvider;
import de.hawlandshut.sdeiss.mobiletoken.crypto.EncryptedSharedSecret;
import de.hawlandshut.sdeiss.mobiletoken.util.Base32;
import de.hawlandshut.sdeiss.mobiletoken.util.Util;


/**
 * Android Activity to setup Secure Mobile Token.
 * 
 * @author Sebastian Deiss
 *
 */
public class SetupActivity extends Activity
{
	private final String TAG = "SetupActivity";
	EditText sharedSecretTextView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);
		
		// Setup button
		final Button setupButton = (Button) findViewById(R.id.buttonSetup);
		final View.OnClickListener submitButtonHandler = new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				doSetup(v);
			}
		};
		
		setupButton.setOnClickListener(submitButtonHandler);
		
		// Scan QR code button
		final Button scanButton = (Button) findViewById(R.id.buttonScan);
		final View.OnClickListener scanButtonHandler = new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				showDialogSecurityWarning();
			}
		};
		scanButton.setOnClickListener(scanButtonHandler);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setup, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_about)
		{
			Intent aboutIntent = new Intent(getApplicationContext(), AboutActivity.class);
			startActivity(aboutIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed()
	{
		finish();
	}
	
	
	/**
	 * Perform setup.
	 * 
	 * @param v The view that contains the data to process during setup
	 */
	private void doSetup(final View v)
	{
		// Initialize Android Shared Preferences
		Preferences.init(getApplicationContext());
		
		// Get the shared secret from the form
		String sharedSecret = ((EditText) findViewById(R.id.editTextSharedSecret)).getText().toString();
		
		// Get the passwords from the form
		final String password = ((EditText) findViewById(R.id.editTextPassword)).getText().toString();
		final String passwordConfirm = ((EditText) findViewById(R.id.editTextPasswordConfirm)).getText().toString();
		
		if (!Util.checkPassword(password))
		{
			// Check if the password meets the requirements
			Log.d(TAG, "Password does not meet the requirements!");
			this.showDialogInvalidPassword(1);
		}
		else if (!password.equals(passwordConfirm))
		{
			// Check passwords for match
			Log.d(TAG, "The submitted password do not match!");
			this.showDialogInvalidPassword(2);
		}
		else
		{
			// Save it to Android Shared Preferences
			try
			{
				final String decodedSharedSecret = Util.bytesToHex(Base32.decode(sharedSecret));
				CryptoProvider cp = new EncryptedSharedSecret(password);
				Log.d(TAG, "Using cipher " + cp.getCipherName() + " for encryption");
				
				cp.encrypt(decodedSharedSecret.getBytes());
				sharedSecret = null;
				Preferences.setSharedSecret(cp.getCiphertextHex());
				Preferences.setSalt(cp.getSaltasHex());
				Preferences.setIV(Util.bytesToHex(cp.getIV()));
				Log.d(TAG, "Shared secret sucessfully saved.");
				// Now get back to MainActivity
				Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
				startActivity(mainIntent);
				finish();
			}
			catch (InvalidKeyException IkEx)
			{
				Log.e(TAG, IkEx.getMessage());
			}
		}
	}
	
	
	/**
	 * Alert dialog that gets shown if there is something wrong with the password.
	 * 
	 * @param reason The reason code why the password is not valid.
	 * 				 1: Password does not meet the requirements!
	 * 				 2: The submitted password do not match
	 */
	private void showDialogInvalidPassword(final int reason)
	{
		// Create the alert dialog
		AlertDialog.Builder unlockAlert = new AlertDialog.Builder(this);
		unlockAlert.setTitle(R.string.dialog_pw_check_failed_title);
		if (reason == 1)
			unlockAlert.setMessage(R.string.dialog_pw_check_failed_invalid);
		else
			unlockAlert.setMessage(R.string.dialog_pw_check_failed_not_match);
		
		unlockAlert.setIcon(android.R.drawable.ic_dialog_alert);
		
		// Create the button for the dialog
		unlockAlert.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				// Do nothing
			}
		});
		
		// Finish dialog setup and show it
		AlertDialog dialog = unlockAlert.create();
		dialog.show();
	}
	
	/**
	 * Dialog to inform the user that importing the shared secret as QR code
	 * through a third-party App is insecure.
	 */
	private void showDialogSecurityWarning()
	{
		AlertDialog.Builder securityAlert = new AlertDialog.Builder(this);
		securityAlert.setTitle(R.string.dialog_security_warning_title);
		securityAlert.setMessage(R.string.dialog_security_warning_msg);
		securityAlert.setIcon(android.R.drawable.ic_dialog_alert);
		
		// OK button
		securityAlert.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				openQRCodeScanner();
			}
		});
		
		// Cancel button
		securityAlert.setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				// Do nothing
			}
		});
		
		// Finish dialog setup and show it
		AlertDialog dialog = securityAlert.create();
		dialog.show();
	}
	
	/**
	 * Use Googles zxing QR code scanner to import the shared secret through a QR code.
	 */
	private void openQRCodeScanner()
	{
		Intent intent = new Intent("com.google.zxing.client.android.SCAN");
		intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
		startActivityForResult(intent, 0);
	}
	
	/**
	 * Get the result of the QR code scan and write it in the form field for the
	 * shared secret.
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		if (requestCode == 0 && resultCode == RESULT_OK )
		{
			sharedSecretTextView = ((EditText) findViewById(R.id.editTextSharedSecret));
			sharedSecretTextView.setText(intent.getStringExtra("SCAN_RESULT"));
		}
		else
			Log.e(TAG, "QR code scan failed");
	}
}
