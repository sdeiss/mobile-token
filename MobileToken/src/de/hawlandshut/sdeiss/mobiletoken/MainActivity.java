/*
 * 
 * This file is a part of Secure Mobile Token.
 * Copyright (C) 2015 by Sebastian Deiss, all rights reserved. 
 * 
 */


package de.hawlandshut.sdeiss.mobiletoken;


import java.security.InvalidKeyException;
import java.util.Timer;
import java.util.TimerTask;

import org.bouncycastle.crypto.CryptoException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import de.hawlandshut.sdeiss.mobiletoken.crypto.CryptoProvider;
import de.hawlandshut.sdeiss.mobiletoken.crypto.EncryptedSharedSecret;
import de.hawlandshut.sdeiss.mobiletoken.crypto.TOTP;
import de.hawlandshut.sdeiss.mobiletoken.util.Util;


/**
 * This is where all the action takes place
 * 
 * @author Sebastian Deiss
 *
 */
public class MainActivity extends Activity 
{
	private final String TAG = "MainActivity";
	
	String encryptedSharedSecret;
	String salt;
	String IV;
	
	final String OTP_LENGTH = "6";
	final long T0 = 0;
	final int X = 30;				// Time a OTP is valid in seconds
	String timeSteps = "0";
	String OTP = "";
	TextView OTPTextView;
	TextView TimeTextView;
	int counter = 0;
	String sharedSecret = null;
	Timer t;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Initialize the preferences
		Preferences.init(getApplicationContext());
		
		// Check if a shared secret was saved
		if(!Preferences.getSharedSecret().isEmpty())
		{
			Log.d(TAG, "Secure Mobile Token is ready to operate");
			
			encryptedSharedSecret = Preferences.getSharedSecret();
			salt = Preferences.getSalt();
			IV = Preferences.getIV();
			
			// Get OTP text view
			OTPTextView = (TextView)findViewById(R.id.textViewOTP);
			
			// Get time text view
			TimeTextView = (TextView)findViewById(R.id.textViewTimeRemaining);
			
			// Open dialog the unlock the shared secret
			// The OTP will be generated and displayed in the dialog
			this.showDialogUnlock(encryptedSharedSecret, salt, IV);
			
			// Initial value of remaining time text view
			// Remaining time is between 0..29 
			TimeTextView.setText(Long.toString(X - 1) + " " + getText(R.string.remaining));
		}
		else
		{
			// No shared secret set -> Jump to setup activity
			Log.d(TAG, "No shared secret saved -> Jump to setup activity");
			Intent setupIntent = new Intent(getApplicationContext(), SetupActivity.class);
			startActivity(setupIntent);
			finish();
		}
		
		// Deal with the exit button
		final Button exitButton = (Button) findViewById(R.id.buttonExit);
		final View.OnClickListener exitButtonHandler = new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.d(TAG, "Exiting Secure Mobile Token");
				finish();
			}
		};
		exitButton.setOnClickListener(exitButtonHandler);
		
		// Deal with clipboard copy button
		final Button copyButton = (Button) findViewById(R.id.buttonClipboardCopy);
		final View.OnClickListener copyButtonHandler = new View.OnClickListener()
		{	
			@Override
			public void onClick(View v)
			{
				if (OTPTextView.getText().toString() != null)
				{
					final String otp = OTPTextView.getText().toString();
					final ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
					final ClipData cd = ClipData.newPlainText("Token", otp.replaceAll("\\s+",""));
					clipboard.setPrimaryClip(cd);
					Log.d(TAG, "Copy to clipboard");
					
					/*
					 * Display a message (Toast) to inform the user that the shared secret
					 * has been saved to the clipboard.
					 */
					Toast.makeText(getApplicationContext(), R.string.toast_copy, Toast.LENGTH_SHORT).show();
				}
				else
					Log.e(TAG, "OTP is not set");
			}
		};
		copyButton.setOnClickListener(copyButtonHandler);
	}
	
	@Override
	public void finish()
	{
		sharedSecret = null;
		salt = null;
		IV = null;
		OTP = null;
		if (t != null)
			t.cancel();
		
		super.finish();
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();
	}
	
	@Override
	protected void onRestart()
	{
		super.onRestart();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_delete)
		{
			this.showDialogDelete(false);
			return true;
		}
		if (id == R.id.action_about)
		{
			// Redirect to AboutActivity
			Intent aboutIntent = new Intent(getApplicationContext(), AboutActivity.class);
			startActivity(aboutIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed()
	{
		Log.d(TAG, "Exiting Secure Mobile Token thorugh back button");
		finish();
	}
	
	/*
	 * Dialog(s)
	 * ********************************************************************************* 
	 */
	/**
	 * Dialog to unlock the shared secret in order to use the App.
	 */
	private void showDialogUnlock(final String encryptedSharedSecret, final String salt, final String IV)
	{
		final EditText EditTextPassword = new EditText(this);
		
		// Treat the entered text as password
		EditTextPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		
		// Create the alert dialog
		AlertDialog.Builder unlockAlert = new AlertDialog.Builder(this);
		unlockAlert.setTitle(R.string.dialog_unlock_title);
		unlockAlert.setView(EditTextPassword);
		unlockAlert.setMessage(R.string.dialog_unlock_msg);
		unlockAlert.setIcon(android.R.drawable.ic_dialog_alert);
		
		// Create the buttons for the dialog
		unlockAlert.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				final String password = EditTextPassword.getText().toString();
				try
				{
					// Unlock the shared secret
					Log.d(TAG, "Decrypting shared secret");
					CryptoProvider cp = new EncryptedSharedSecret(password, Util.hexToBytes(salt), Util.hexToBytes(IV));
					Log.d(TAG, "Using cipher " + cp.getCipherName() + " for decryption");
					cp.decrypt(Util.hexToBytes(encryptedSharedSecret));
					sharedSecret = cp.getPlaintext();
					// Run the TOTP schedule that displays the OTP and the ramining time
					TOTPSchedule();
				}
				catch (InvalidKeyException IkEx)
				{
					Log.d(TAG, "Exception when decrypting shared secret");
					Log.e(TAG, IkEx.getMessage());
				}
				catch(CryptoException cex)
				{
					Log.i(TAG, "Decryption failed due to invalid password.");
					showDialogInvalidPassword();
				}
			}
		});
		
		unlockAlert.setNegativeButton(getString(R.string.dialog_forgot_password), new DialogInterface.OnClickListener()
		{
			
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				showDialogDelete(true);
			}
		});
		
		// Finish dialog setup and show it
		AlertDialog dialog = unlockAlert.create();
		dialog.setOnShowListener(new OnShowListener()
		{
			// Shows keyboard with dialog
			public void onShow(DialogInterface dialog)
			{
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(EditTextPassword, InputMethodManager.SHOW_IMPLICIT);
			}
		});

		dialog.setCancelable(false);
		dialog.show();
	}
	
	/**
	 * Dialog that informs the user that he entered a invalid password to unlock the App.
	 */
	private void showDialogInvalidPassword()
	{
		AlertDialog.Builder badPasswordAlert = new AlertDialog.Builder(this);
		badPasswordAlert.setTitle(R.string.dialog_invalid_pw_title);
		badPasswordAlert.setMessage(R.string.dialog_invalid_pw_msg);
		badPasswordAlert.setIcon(android.R.drawable.ic_dialog_alert);
		
		badPasswordAlert.setPositiveButton(R.string.dialog_try_again, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				showDialogUnlock(encryptedSharedSecret, salt, IV);
			}
		});
		
		AlertDialog dialog = badPasswordAlert.create();
		dialog.setCancelable(false);
		dialog.show();
	}
	
	/**
	 * Dialog to delete the shared secret.
	 * 
	 * @param fromUnlockDialog <code>true</code> if this dialog was opened by the
	 * 						   unlock dialog.
	 */
	private void showDialogDelete(final boolean fromUnlockDialog)
	{
		AlertDialog.Builder deleteAlert = new AlertDialog.Builder(this);
		deleteAlert.setTitle(R.string.dialog_delete_title);
		deleteAlert.setMessage(R.string.dialog_delete_msg);
		deleteAlert.setIcon(android.R.drawable.ic_dialog_alert);
		
		deleteAlert.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener()
		{	
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				// Delete the shared secret and redirect to the SetupActivity
				deleteSharedSecret();
				Toast.makeText(getApplicationContext(), R.string.toast_deleted, Toast.LENGTH_LONG).show();
				Intent setupIntent = new Intent(getApplicationContext(), SetupActivity.class);
				startActivity(setupIntent);
				finish();
			}
		});
		
		deleteAlert.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener()
		{	
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				if (fromUnlockDialog)
					finish();
				// Do nothing
			}
		});
		
		AlertDialog dialog = deleteAlert.create();
		dialog.setCancelable(false);
		dialog.show();
	}
	
	
	/*
	 * Internals
	 * ******************************************************************************** 
	 */
	
	/**
	 * Return the current time.
	 * 
	 * @return The current time as String
	 */
	private String getCurrentTime()
	{
		final long time = System.currentTimeMillis() / 1000;
		final long T = (time - T0) / X;
		return Long.toHexString(T);
	}
	
	/**
	 * Compute the TOTP and it's remaining time and update it if necessary
	 */
	private void TOTPSchedule()
	{
		timeSteps = getCurrentTime();
		OTP = TOTP.generateTOTP(sharedSecret, timeSteps, OTP_LENGTH);
		// Display OTP
		OTPFormatAndSet(OTP);
		
		// Display remaining time and reset OTP
		t = new Timer();
		t.scheduleAtFixedRate(new TimerTask()
		{	
			@Override
			public void run()
			{
				runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						// Remaining time is between 0..29 
						long remainingTime = X - System.currentTimeMillis()%30000/1000 - 1;
						TimeTextView.setText(Long.toString(remainingTime) + " " + getText(R.string.remaining));
						if (remainingTime == 0)
						{
							Thread.currentThread();
							try
							{
								Thread.sleep(1000);
							}
							catch (InterruptedException e)
							{
								Log.d(TAG, e.getMessage());
							}
							String timeSteps = getCurrentTime();
							OTP = TOTP.generateTOTP(sharedSecret, timeSteps, OTP_LENGTH);
							OTPFormatAndSet(OTP);
						}
					}
				});
			}
		}, 1000, 1000);
	}
	
	/**
	 * Split the OTP in two strings of the same size. This makes it easier to read.
	 * 
	 * @param otp The OTP to fromat
	 */
	private void OTPFormatAndSet(final String otp)
	{
		final int otpLength = Integer.parseInt(OTP_LENGTH);
		String otpPart1 = otp.substring(0, otpLength / 2);
		String otpPart2 = otp.substring(otpLength / 2, otp.length());
		OTPTextView.setText(otpPart1 + " " + otpPart2);
	}
	
	/**
	 * Delete the shared secret from the preferences.
	 */
	private void deleteSharedSecret()
	{
		Preferences.setSharedSecret("");
		Preferences.setSalt("");
		Preferences.setIV("");
		if (t != null)
			t.cancel();
	}
}
