/*
 * 
 * This file is a part of Secure Mobile Token.
 * Copyright (C) 2015 by Sebastian Deiss, all rights reserved. 
 * 
 */


package de.hawlandshut.sdeiss.mobiletoken;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/**
 * Android Shared Preferences for Secure Mobile Token.
 * 
 * @author Sebastian Deiss
 *
 */
public class Preferences
{
	private static SharedPreferences preferences;
	private static SharedPreferences.Editor editor;

	/*
	 * The shared secret used for OTP generation with TOTP (XORed with the users password=
	 */
	private static final String PROPERTY_SHARED_SECRET = "SHARED_SECRET";
	/*
	 * The IV used for encryption in CBC mode.
	 */
	private static final String PROPERTY_IV = "IV";
	/*
	 * The salt used for key derivation
	 */
	private static final String PROPERTY_SALT = "SALT";
	
	/**
	 * Initializes the Android PreferenceManager.
	 * 
	 * @param ctx context of the activity using the SharedPreferences (MainActivity)
	 */
	@SuppressLint("CommitPrefEdits")
	public static void init(final Context ctx)
	{
		preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
		editor = preferences.edit();
	}
	
	
	/**
	 * Getter for the shared secret.
	 * 
	 * @return Returns the shared secret from the Preferences.
	 */
	public static String getSharedSecret()
	{
		return preferences.getString(PROPERTY_SHARED_SECRET, "");
	}
	
	/**
	 * Getter for the initialization vector.
	 * 
	 * @return Returns the IV used for encryption
	 */
	public static String getIV()
	{
		return preferences.getString(PROPERTY_IV, "");
	}
	
	/**
	 * Getter for the salt.
	 * 
	 * @return The salt used for key derivation
	 */
	public static String getSalt()
	{
		return preferences.getString(PROPERTY_SALT, "");
	}
	
	/**
	 * Write the shared secret to the Shared Preferences (setter)
	 * 
	 * @param sharedSecret The shared secret (encoded)
	 * @see CryptoUtil
	 */
	public static void setSharedSecret(final String sharedSecret)
	{
		editor.putString(PROPERTY_SHARED_SECRET, sharedSecret);
		editor.commit();
	}
	
	/**
	 * Write the IV to Shared Preferences
	 * 
	 * @param IV The initialization vector used for encryption
	 */
	public static void setIV(final String IV)
	{
		editor.putString(PROPERTY_IV, IV);
		editor.commit();
	}
	
	/**
	 * Write the salt to Shared Preferences.
	 * 
	 * @param salt The salt used for key derivation
	 */
	public static void setSalt(final String salt)
	{
		editor.putString(PROPERTY_SALT, salt);
		editor.commit();
	}
}
