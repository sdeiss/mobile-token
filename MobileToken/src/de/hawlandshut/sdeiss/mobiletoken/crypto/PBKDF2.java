/*
 * 
 * This file is a part of Secure Mobile Token.
 * Copyright (C) 2015 by Sebastian Deiss, all rights reserved. 
 * 
 */

package de.hawlandshut.sdeiss.mobiletoken.crypto;

import java.security.SecureRandom;
import java.security.InvalidKeyException;
import java.util.Arrays;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.digests.WhirlpoolDigest;

import de.hawlandshut.sdeiss.mobiletoken.util.Util;

/**
 * PKCS #5 Password Based Key Derivation Framework 2.0 (PBKDF2)
 * 
 * @author Sebastian Deiss
 * @see <a href="https://www.ietf.org/rfc/rfc2898.txt">RFC 2898</a>
 *
 */
public final class PBKDF2
{
	private final HMac PRF;
	private byte[] salt;
	private final int hLen;
	
	/**
	 * Initialize PBKDF2
	 * 
	 * @param PRFName The name of the hash algorithm to use with HMAC as PRF
	 * @throws UnsupportedHashAlgorithmException If the supplied hash algorithm name does not match any supported hash algorithm.
	 */
	public PBKDF2()
	{
		this.PRF = new HMac(new WhirlpoolDigest());
		
		// Setup
		this.hLen = this.PRF.getMacSize();
		this.salt = new byte[this.PRF.getMacSize()];
		
		// Generate a salt
		new SecureRandom().nextBytes(salt);
	}
	
	/**
	 * Initialize PBKDF2 with a specified salt
	 * 
	 * @param PRFName The name of the hash algorithm to use with HMAC as PRF
	 * @param salt The salt to use
	 * @throws UnsupportedHashAlgorithmException If the supplied hash algorithm name does not match any supported hash algorithm.
	 */
	public PBKDF2(final byte[] salt)
	{
		this();
		this.salt = salt;
	}
	
	
	/**
	 * Get the PRF name.
	 * 
	 * @return The name of the PRF as string
	 */
	public String getPRFName()
	{
		return this.PRF.getAlgorithmName();
	}
	
	/**
	 * Get the length of the output of the PRF.
	 * 
	 * @return The length of the PRF output
	 */
	public int getPRFSize()
	{
		return this.PRF.getMacSize();
	}
	
	/**
	 * Get the salt used to derive the key.
	 * 
	 * @return The salt used to derive the key
	 */
	public byte[] getSalt()
	{
		return this.salt;
	}
	
	/**
	 * Derive a key.
	 * 
	 * @param password The password to derive the key from
	 * @param iterCount The iteration count
	 * @return Returns a key derived with the specified parameters
	 * @throws InvalidKeyException If the specified length for the derived key is too long
	 */
	public byte[] deriveKey(final String password, final int iterCount) throws InvalidKeyException
	{
		return this.deriveKey(password, iterCount, this.hLen);
	}
	
	/**
	 * Derive a key with a specified length.
	 * 
	 * @param password The password to derive the key from
	 * @param iterations The iteration count
	 * @param dkLen The length of the derived key
	 * @return Returns a key derived with the specified parameters
	 * @throws InvalidKeyException If the specified length for the derived key is too long
	 */
    public byte[] deriveKey(final String password, final int iterations, final int dkLen) throws InvalidKeyException
    {
    	// Check key length
    	if (dkLen > ((Math.pow(2, 32) - 1) * this.hLen))
    		throw new InvalidKeyException("Derived key too long");
    			
    	byte[] derivedKey = new byte[dkLen];
    	
    	final int J = 0;
        final int K = this.PRF.getMacSize();
        final int U = this.PRF.getMacSize() << 1;
        final int B = K + U;
        final byte[] workingArray = new byte[K + U + 4];

        // Initialize PRF
     	CipherParameters macParams = new KeyParameter(password.getBytes());
     	this.PRF.init(macParams);

     	// Perform iterations
        for (int kpos = 0, blk = 1; kpos < dkLen; kpos += K, blk++)
        {
            Util.storeInt32BE(blk, workingArray, B);

            this.PRF.update(this.salt, 0, salt.length);
            
            this.PRF.reset();
            this.PRF.update(salt, 0, salt.length);
            this.PRF.update(workingArray, B, 4);
            this.PRF.doFinal(workingArray, U);
            System.arraycopy(workingArray, U, workingArray, J, K);

            for (int i = 1, j = J, k = K; i < iterations; i++)
            {
            	this.PRF.init(macParams);
            	this.PRF.update(workingArray, j, K);
            	this.PRF.doFinal(workingArray, k);

                for (int u = U, v = k; u < B; u++, v++)
                {
                	workingArray[u] ^= workingArray[v];
                }

                int swp = k;
                k = j;
                j = swp;
            }

            int tocpy = Math.min(dkLen - kpos, K);
            System.arraycopy(workingArray, U, derivedKey, kpos, tocpy);
        }

        Arrays.fill(workingArray, (byte)0);

        return derivedKey;
    }
}
