/*
 * 
 * This file is a part of Secure Mobile Token.
 * Copyright (C) 2015 by Sebastian Deiss, all rights reserved. 
 * 
 */

package de.hawlandshut.sdeiss.mobiletoken.crypto;

import java.security.InvalidKeyException;

import org.bouncycastle.crypto.CryptoException;

/**
 * Wrapper around the bouncycastle crypto library to encrypt a piece of text.
 * 
 * @author Sebastian Deiss
 *
 */
public interface CryptoProvider
{	
	/**
	 * Get the name of the used cipher.
	 * 
	 * @return Returns the name of the used cipher
	 */
	public String getCipherName();
	
	/**
	 * Get the plain shared secret.
	 * 
	 * @return The shared secret plaintext
	 */
	public String getPlaintext();
	
	/**
	 * Get the encrypted shared secret.
	 * 
	 * @return The shared secret ciphertext as hex string
	 */
	public String getCiphertextHex();
	
	/**
	 * Getter for the salt.
	 * 
	 * @return The salt used for key derivation
	 */
	public String getSaltasHex();
	
	/**
	 * Get the initialization vector used in CBC mode.
	 * 
	 * @return The IV
	 */
	public byte[] getIV();
	
	/**
	 * Encrypt a piece of text.
	 * 
	 * @param plaintext The text as a byte array
	 * @throws InvalidKeyException If an error occurs during key derivation
	 */
	public void encrypt(final byte[] plaintext) throws InvalidKeyException;
	
	/**
	 * Decrypt a piece of text.
	 * 
	 * @param ciphertext The encrypted text as byte array
	 * @throws InvalidKeyException If an error occurs during key derivation
	 * @throws CryptoException If decryption was not successful due to invalid key
	 */
	public void decrypt(final byte[] plaintext) throws InvalidKeyException, CryptoException;
}
