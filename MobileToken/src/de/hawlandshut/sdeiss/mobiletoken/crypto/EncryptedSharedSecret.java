/*
 * 
 * This file is a part of Secure Mobile Token.
 * Copyright (C) 2015 by Sebastian Deiss, all rights reserved. 
 * 
 */

package de.hawlandshut.sdeiss.mobiletoken.crypto;

import java.security.InvalidKeyException;
import java.security.SecureRandom;

import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;

import de.hawlandshut.sdeiss.mobiletoken.util.Util;

/**
 * Wrapper around the bouncycastle crypto library to encrypt a piece of text
 * with a password using AES-256-CBC and PBKDF2 with HMAC-Whirlpool as PRF.
 * 
 * @author Sebastian Deiss
 *
 */
public final class EncryptedSharedSecret implements CryptoProvider
{
	private static final byte[] MAGIC = "4f4b".getBytes();	// Hex OK
	private static final int KEY_LENGTH = 32;				// 256 bit key
	
	private final int iterationCount = 5000;
	private final int blockSize;
	
	private BlockCipher cipherInstance;
	private PBKDF2 kdf;
	private String encryptedSharedSecret;
	private String sharedSecret;
	private String password;
	private byte[] salt;
	private byte[] IV;
	
	/**
	 * Create a new encrypted shared secret.
	 * 
	 * @param password The password to encrypt / decrypt the shared secret
	 * @throws InvalidKeyException
	 */
	public EncryptedSharedSecret(final String password)  throws InvalidKeyException
	{
		this.password = password;
		this.cipherInstance = new CBCBlockCipher(new AESFastEngine());
		this.blockSize = this.cipherInstance.getBlockSize();
		this.IV = new byte[this.blockSize];
		this.kdf = new PBKDF2();
		this.salt = kdf.getSalt();
		
		new SecureRandom().nextBytes(IV);
	}
	
	/**
	 * Create a new encrypted shared secret.
	 * 
	 * @param password The password to encrypt / decrypt the shared secret
	 * @param salt The salt for key derivation
	 */
	public EncryptedSharedSecret(final String password, final byte[] salt, final byte[] IV)  throws InvalidKeyException
	{
		this.cipherInstance = new CBCBlockCipher(new AESFastEngine());
		this.kdf = new PBKDF2(salt);
		this.blockSize = this.cipherInstance.getBlockSize();
		this.password = password;
		this.IV = new byte[this.blockSize];
		this.salt = salt;
		this.IV = IV;
	}
	
	
	/**
	 * Get the name of the used cipher.
	 * 
	 * @return Returns the name of the used cipher
	 */
	public String getCipherName()
	{
		return this.cipherInstance.getAlgorithmName();
	}
	
	/**
	 * Get the plain shared secret.
	 * 
	 * @return The shared secret plaintext
	 */
	@Override
	public String getPlaintext()
	{
		return this.sharedSecret;
	}
	
	/**
	 * Get the encrypted shared secret.
	 * 
	 * @return The shared secret ciphertext as hex string
	 */
	@Override
	public String getCiphertextHex()
	{
		return this.encryptedSharedSecret;
	}
	
	/**
	 * Getter for the salt.
	 * 
	 * @return The salt used for key derivation
	 */
	@Override
	public String getSaltasHex()
	{
		return Util.bytesToHex(this.salt);
	}
	
	/**
	 * Get the initialization vector used in CBC mode.
	 * 
	 * @return The IV
	 */
	@Override
	public byte[] getIV()
	{
		return this.IV;
	}
	
	@Override
	protected void finalize() throws Throwable
	{
		this.encryptedSharedSecret = null;
		this.IV = null;
		this.cipherInstance = null;
		this.kdf = null;
		this.salt = null;
		super.finalize();
	}
	
	/**
	 * Encrypt the shared secret.
	 * 
	 * @param plaintext The shared secret as byte array
	 * @throws InvalidKeyException If an error occurs during key derivation
	 */
	@Override
	public void encrypt(final byte[] plaintext) throws InvalidKeyException
	{
		CipherParameters params = this.createKey();
		this.cipherInstance.init(true, params);
		
		byte[] toEncrypt = new byte[MAGIC.length + plaintext.length];
		byte[] ciphertext = new byte[toEncrypt.length + toEncrypt.length % this.blockSize];
		
		System.arraycopy(MAGIC, 0, toEncrypt, 0, MAGIC.length);
		System.arraycopy(plaintext, 0, toEncrypt, MAGIC.length, plaintext.length);
		
		// Check if padding is required
		final int paddingLength = this.blockSize - toEncrypt.length % blockSize;
		byte[] toEncryptPadded = new byte[toEncrypt.length + paddingLength];
		if (paddingLength != 0)
		{
			System.arraycopy(toEncrypt, 0, toEncryptPadded, 0, toEncrypt.length);
			PKCS7Padding padding = new PKCS7Padding();
			padding.init(new SecureRandom());
			padding.addPadding(toEncryptPadded, toEncrypt.length);

			for (int i = 0; i < toEncryptPadded.length; i+=this.blockSize)
				this.cipherInstance.processBlock(toEncryptPadded, i, ciphertext, i);
		}
		else
		{
			for (int i = 0; i < toEncryptPadded.length; i+=this.blockSize)
				this.cipherInstance.processBlock(toEncrypt, i, ciphertext, i);
		}
		
		this.encryptedSharedSecret = Util.bytesToHex(ciphertext);
	}
	
	/**
	 * Decrypt a shared secret.
	 * 
	 * @param ciphertext The encrypted shared secret as byte array
	 * @throws InvalidKeyException If an error occurs during key derivation
	 * @throws CryptoException If decryption was not successful due to invalid key
	 */
	@Override
	public void decrypt(final byte[] ciphertext) throws InvalidKeyException, CryptoException
	{
		CipherParameters params = this.createKey();
		this.cipherInstance.init(false, params);
		byte[] plaintextPadded = new byte[ciphertext.length];
		
		for (int i = 0; i < ciphertext.length; i+=this.blockSize)
			this.cipherInstance.processBlock(ciphertext, i, plaintextPadded, i);
		
		// Remove padding
		PKCS7Padding padding = new PKCS7Padding();
		padding.init(new SecureRandom());
		final int paddingBytes = padding.padCount(plaintextPadded);
		byte[] plaintext = new byte[plaintextPadded.length - paddingBytes];
		System.arraycopy(plaintextPadded, 0, plaintext, 0, plaintext.length);
		
		String result = new String(plaintext);
		// Check decryption result
		if (result.startsWith(new String(MAGIC)))
		{
			this.sharedSecret = result.substring(MAGIC.length, result.length());
		}
		else
			throw new CryptoException("Decryption failed");
	}
	
	/**
	 * Create the encryption / decryption key.
	 * 
	 * @return Returns a {@code CipherParameters} instance that contains the key and the IV
	 * @throws InvalidKeyException If an error occurs during key derivation
	 */
	private CipherParameters createKey() throws InvalidKeyException
	{
		// Derive the encryption key
		byte[] userkey = this.kdf.deriveKey(this.password, this.iterationCount, KEY_LENGTH);
		this.password = null;
		return new ParametersWithIV(new KeyParameter(userkey), this.IV);
	}
}