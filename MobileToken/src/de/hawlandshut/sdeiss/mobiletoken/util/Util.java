/*
 * 
 * This file is a part of Secure Mobile Token.
 * Copyright (C) 2015 by Sebastian Deiss, all rights reserved. 
 * 
 */


package de.hawlandshut.sdeiss.mobiletoken.util;

/**
 * Utility functions for Secure Mobile Token.
 * 
 * @author Sebastian Deiss
 *
 */
public class Util
{
	/**
	 * Check if the password matches the requirements.
	 * 
	 * @param password The password to check
	 * @return True if the password matches the requirements otherwise false
	 */
	public static boolean checkPassword(final String password)
	{
		if ((password.length() >= 4) && password.matches(".*\\d.*") && password.matches(".*[A-Za-z].*"))
			return true;
		else
			return false;
	}
	
	/**
	 * Convert a byte array to a String of hex characters.
	 * 
	 * @param bytes The byte array to convert
	 * @return A hex string
	 */
	public static String bytesToHex(final byte[] bytes)
	{
		if (bytes == null)
			return null;
		else
		{
			int length = bytes.length;
			String hexBytes = "";
			for (int i = 0; i < length; i++)
			{
				if ((bytes[i] & 0xFF) < 16)
				{
					hexBytes += "0";
					hexBytes += java.lang.Integer.toHexString(bytes[i] & 0xFF);
				}
				else
					hexBytes += java.lang.Integer.toHexString(bytes[i] & 0xFF);
			}
			
			return hexBytes;
		}
	}
	
	/**
	 * Convert a String of hex characters to a byte array.
	 * 
	 * @param hexBytes The string to convert
	 * @return A byte array
	 */
	public static byte[] hexToBytes(final String hexBytes)
	{
		if (hexBytes == null | hexBytes.length() < 2)
			return null;
		else
		{
			int length = hexBytes.length() / 2;
			byte[] buffer = new byte[length];
			for (int i = 0; i < length; i++)
				buffer[i] = (byte) Integer.parseInt(hexBytes.substring(i * 2, i * 2 + 2), 16);
			
			return buffer;
		}
	}
	
	/**
	 * Convert a 32-bit integer value into a big-endian byte array
	 * 
	 * @param value The integer value to convert
	 * @param bytes The byte array to store the converted value
	 * @param offSet The offset in the output byte array
	 */
	public final static void storeInt32BE(int value, byte[] bytes, int offSet)
	{
		bytes[offSet + 3] = (byte)(value       );
		bytes[offSet + 2] = (byte)(value >>>  8);
		bytes[offSet + 1] = (byte)(value >>> 16);
		bytes[offSet    ] = (byte)(value >>> 24);
    }
}
