# Secure Mobile Token #
Use your Android device to generate OTPs with TOTP for two-factor authentication.
Secure Mobile Token is distributed under the terms of the BSD 2-Clause License. For details see [License.txt](./License.txt). 

## Requirements ##
Android >= 4.2 (Jelly Bean)


## Security ##
The shared secret used to create the OTP will be encrypted with a password you choose before saving it to Shared Preferences.
AES-256-CBC is the cipher for encryption and for key derivation PBKDF2 (PKCS #5) with 5.000 iterations and HMAC-Whirlpool as PRF is used.
This App dos not require any specific permissions since it works offline and dos not need any storage.


## Issues ##
Please report bugs to https://bitbucket.org/sdeiss/mobile-token/issues.
